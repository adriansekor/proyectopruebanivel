<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//Modelo para la tabla table_contacto
class Contacto extends Model
{

	 protected $table = "table_contacto";//nombre de la tabla de la bdd
    

    protected $fillable = [ //campos a tratar en el sistema.
        'Nombre', 'Apellido', 'Email'
    ];



}
