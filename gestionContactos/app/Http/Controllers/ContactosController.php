<?php

namespace App\Http\Controllers;

use App\Contacto;
use Illuminate\Http\Request;
use App\Exports\ContactExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;


class ContactosController extends Controller
{

    public function index()//redirecciona a indice
    {
        $contacto= Contacto::all();
        return view('index')->with('contacto', $contacto);
    }

     public function getContacto()
    {
        return Contacto::all();
    }

    public function delete_view($id)//redirecciona a delete con el identificador de contacto
    {
      return view('delete')->with('id', $id);
    }


    public function create()//redirecciona a la pagina de crear un nuevo contacto
    {
        return view('create');
    }

      public function store(Request $request)//crea un nuevo contacto
    {
        $contacto = new Contacto;
        $contacto->Nombre=$request->input('Nombre');
        $contacto->Apellido=$request->input('Apellido');
        $contacto->Email=$request->input('Email');
        $contacto-> save();
          return redirect()->route('contactos.index');
    }

    public function edit($id){//redirecciona a la pagina edit y devuelve el contacto a editar.
    	$contacto= Contacto::find($id);
    	return view ('edit')->with('contacto', $contacto);
    }


    public function update(Request $request, $id)//Actualiza un contacto
    {
        $contacto= Contacto::find($id);
        $contacto->Nombre=$request->input('nombre');
        $contacto->Apellido=$request->input('apellido');
        $contacto->Email=$request->input('email');
        $contacto->save();

        return redirect()->route('contactos.index');
    }

    public function destroy($id)//Elimina un contacto
    {
         Contacto::destroy($id);
        return redirect()->route('contactos.index');
    }

       public function show($id)
    {
        $contacto=Contacto::find($id);
        return  view('index',compact('contacto'));
    }
}
