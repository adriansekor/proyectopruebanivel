<?php

namespace App\Exports;
use App\Contacto;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContactExport implements FromCollection,WithHeadings
{

    public function headings():array
    {
      return[
        'id','Nombre','Apellido','Email'
      ];
}

    public function collection()
    {
        $contact=DB::table('table_contacto')->select('id','Nombre','Apellido','Email')->get();
        return $contact;
      }
  }
