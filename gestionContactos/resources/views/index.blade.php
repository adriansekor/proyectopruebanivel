<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script type="text/javascript" src="{{ URL::asset('/includes/prototype.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('/includes/scriptaculous.js?load=effects') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('/includes/modalbox.js') }}"></script>
        <link rel="stylesheet" href="{{ URL::asset('css/modalbox.css') }}" type="text/css" media="screen" />
         <script type="text/javascript" src="{{ URL::asset('/js/funciones.js') }}"></script>

        <!-- Styles -->
        <style>
        table,td{
            border: 1px solid black;
            text-align: center;
        }
        table{
            margin-left: 36%;
            margin-right: 35%;
        }
        h2{
            color: #B1B0B0;
            text-align: center;
        }
        #add-export-button{
           margin-left: 48%;
            margin-right: 40% ;
        }
        td{
            background-color: #B1B0B0;
        }
        </style>
    </head>
    <body>

<h2>CONTACTOS</h2>



 <div id="resultados"></div>
        <script>
            myFunction();
     function myFunction() {
 setInterval(presionBoton, 500);}
</script>


<br>
<div id="add-export-button">
<a href="/contactos/create" title="Add" onClick="Modalbox.show(this.href, {title: this.title, width: 600}); return false;"><button id="create" type="button">Add</button></a>
</br></br>
<a href="/exportar"><button id="create" type="button">Export in Excell</button></a>
</div>

<div id="tabla">
    
</div>


    </body>
</html>
