/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


addEventListener('load', inicializarEventos, false);

function inicializarEventos() {
    var ob = document.getElementById('boton');
    ob.addEventListener('click', presionBoton, false);
}

function presionBoton() {
    var cadena = '[{"nombre" : "Jordi",' +
            '"apellido" : "Isern",' +
            '"lugar_votacion" : "Reus"},' +
            '{"nombre" : "Pepe",' +
            '"apellido" : "López",' +
            '"lugar_votacion" : "Tarragona"},' +
            '{"nombre" : "Juan",' +
            '"apellido" : "García",' +
            '"lugar_votacion" : "Riudoms"}]';
    var persona = JSON.parse(cadena);
    var dni = document.getElementById("dni").value;
    if (dni == 1 || dni == 2 || dni == 3) {
        document.getElementById("mostrarDatos").innerHTML = 'Nombre: ' + persona[dni - 1].nombre + '<br>Apellido: ' + persona[dni - 1].apellido + '<br>Votar en: ' + persona[dni - 1].lugar_votacion;
    } else{
        alert("Ingresa un dni que sea 1, 2 o 3");
    }
}