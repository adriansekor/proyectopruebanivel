/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


addEventListener('load', inicializarEventos, false);

function inicializarEventos() {
    var ob = document.getElementById('boton');
    ob.addEventListener('click', presionBoton, false);
}

var conexion;
function presionBoton() {
    conexion = new XMLHttpRequest();
    conexion.onreadystatechange = procesarEventos;
    conexion.open('GET', '/cargaJSON', true);
    conexion.send();
}

function procesarEventos() {
    var resultados = document.getElementById("resultados");
    if (conexion.readyState == 4) {
       
        
        var datos = JSON.parse(conexion.responseText);
        var salida = '';
        salida +='<table>';
        for (var i = 0; i < datos.length; i++) {
            salida +='<tr>';
            salida += '<td>Nombre: ' + datos[i].Nombre + "</td>";
            salida += '<td>Apellido: ' + datos[i].Apellido + "</td>";
            salida += '<td>Email: ' + datos[i].Email + "</td>";
            salida += '<td> <a href="/contactos/'+datos[i].id+'/edit/"';
            salida += 'title="Edit" onClick="Modalbox.show(this.href, {title: this.title, width: 600}); return false;">';
            salida+='<button id="edit" type="button">Edit</button></a>';

            salida += ' <a href="/delete/'+datos[i].id+'"';
            salida += 'title="Delete" onClick="Modalbox.show(this.href, {title: this.title, width: 600}); return false;">';
            salida+='<button id="delete" type="button">Delete</button></a></td>';
            salida +='</tr>';
        }
        salida +='</stable>';
        resultados.innerHTML = salida;
    } else {
      
    }
}