<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactosController@index');
Route::get('index', 'ContactosController@index');
Route::get('/delete/{id}','ContactosController@delete_view');
Route::post('/contacto','ContactosController@store');
Route::resource('contactos', 'ContactosController');
Route::delete('/destroy/{id}','ContactosController@destroy');

Route::get('/cargaJSON', function () {
    return view('cargaJSON');
});

Route::get('/exportar','ExportController@export');


