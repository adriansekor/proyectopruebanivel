<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ContactosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('table_contacto')->truncate();

       	//Creacion del primer registro a la tabla table_contacto
        DB::table('table_contacto')->insert([
            'Nombre' => 'John',
            'Apellido' => 'Smith',
            'Email' => 'john.smith@gmail.com',
        ]);

        //creación del segundo registro a la tabla table_contacto
        DB::table('table_contacto')->insert([
            'Nombre' => 'Ann',
            'Apellido' => 'Reily',
            'Email' => 'ann.reily@gmail.com',
        ]);
    }
}
